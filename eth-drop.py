from web3 import Web3
import time
import os
from enum import Enum, auto
from tqdm import tqdm

recipients = [
  "0x0e190Baf2eBaA5322a93A205eD8450D6E893BbbE",
  "0x87e0ed760fb316eeb94bd9cf23d1d2be87ace3d8",
  "0xd4fa489eacc52ba59438993f37be9fcc20090e39",
  "0x760bf27cd45036a6c486802d30b5d90cffbe31fe",
  "0x56a32fff5e5a8b40d6a21538579fb8922df5258c"
]

class NetworkConfig(Enum):
  RINKEBY = auto()
  MAINNET = auto()

  def get_chain_id(self):
    if self.RINKEBY:
      chain_id = 4
    elif self.MAINNET:
      chain_id = 1
    else:
      chain_id = -1
    return chain_id

  def get_provider(self):
    if self.RINKEBY:
      provider = 'https://rinkeby.infura.io/v3/%s' % os.environ['INFURA_API_KEY']
    elif self.MAINNET:
      provider = 'https://mainnet.infura.io/v3/%s' % os.environ['INFURA_API_KEY']
    else:
      provider = 'https://www.google.com'
    return provider

network = NetworkConfig.RINKEBY
test_run = True

w3 = Web3(Web3.HTTPProvider(network.get_provider()))
chain_id = network.get_chain_id()
gas_cost = 60_000
private_key = os.environ['METAMASK_PVT_KEY']
airdrop = 0.01
sender_address = '0x760bf27cd45036a6c486802d30b5d90cffbe31f'

tx_count = w3.eth.getTransactionCount(sender_address)
complete = []

for i in tqdm(range(len(recipients))):
  address = recipients[i]
  if address in complete: continue
  address_checksum = w3.toChecksumAddress(address)

  signed_txn = w3.eth.account.signTransaction(dict(
      nonce=tx_count,
      gasPrice = w3.eth.gasPrice,
      gas = gas_cost,
      to=address_checksum,
      value=w3.toWei(airdrop, 'ether'),
      chainId=chain_id
    ),
    private_key)

  if not test_run:
    w3.eth.sendRawTransaction(signed_txn.rawTransaction)

  print('Sent %s ETH to %s' % (airdrop, address))
  tx_count += 1
  complete.append(address)
